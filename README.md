A simple CSS experiment. Maybe it will become something cool eventurally.
The idea is to create a small, minimalist, yet beautiful css library based on Semantic-UI.

To-do:

- [ ] Experiment with Samantic-UI and determine which style elements we want to use.
- [ ] Create a new CSS file containing only what is needed.
- [ ] Create a light and dark theam.
- [ ] Document everything.
